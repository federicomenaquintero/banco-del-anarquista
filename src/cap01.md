# 1. Plantando la Bandera

Arrastré mi banco dentro del salón de cantina alemana de 1896 y lo solté frente al gabinete gigante empotrado al edificio - llamado el bar de atrás - donde se alguna vez se alinearon botellas de licor duro delante de un espejo de 4 x 10 pies.

Dios, detesto los espejos. Pero volteé a encararlo de todas formas.

Algunos días me pregunto qué fue lo que me trajo a éste momento. Había dejado un prestigioso y bien pagado trabajo haciendo publicidad. Una casa en los suburbios. Cuatro semanas de vacaciones. Y había cambiado aquello por un bar desgastado en una zona roja de Covington, Kentucky. ¿Por qué?

Pienso que tuvo algo que ver que la muerte de mi tío.

Ese día, levanté mi teléfono y tomé un auto-retrato: el banco, el espejo y yo. Éste sitio, aquí donde está mi banco de carpintero, es donde espero morir. Aquí es donde acabará el cuento.

![](./ilustraciones/triple-compas.png)

El cuento inicia cuando tenía 28 años, era inseguro y (honestamente) el peor carpintero que conocía. Mi papá era mejor artesano. ¿Mi tío? Mejor. ¿Mi mejor amigo del momento? Definitivamente mejor. ¿Su esposa? Ella era mucho mejor que todos nosotros - era una natural (FIXME: natural). Pero yo tenía algo que ellos no: un trabajo en una revista sobre carpintería.

Una revista nacional. Con 200,000 lectores. Un puesto que provoca respeto auténtico.

Aquí va una instantánea de uno de mis primeros días, cuando llegó una caja desde California, dirigida hacia a mi.

"No &\*% mereces eso", escupió Jim. Acababa de ver mi _block plane_ (FIXME: block plane) con asco. "No te lo has ganado". Y con eso, se alejó de mi escritorio.

Después de sacudirme del golpazo repentino, decidí que el exabrupto de Jim no era para sorprender. Desde su experiencia, cada cosa en la vida debe ser ganada.

![](./ilustraciones/cap01-staff-box.png)

(FIXME - traducir pie de foto)

Jim y yo eramos editores de bajo nivel, en la revista _Popular Woodworking Magazine_, por ahí de 1997. Como otros colaboradores (excepto yo), Jim había surgido de entre los rangos pasando por talleres comerciales de carpintería en Cincinnati, dónde la revista tenía su oficina. En esos tiempos, había poca seguridad para poder mantener un trabajo en talleres de la localidad. Era común ser despedido cuando disminuía el trabajo antes de navidad y ser re-contratado en la primavera, cuando la gente rica quería cocinas nuevas. La paga apestaba y el trabajo era difícil, con la mayoría de los talleres helados durante el invierno y asando durante el verano.

Así, que sí, pude entender cómo es que un joven periodista (yo) comprando un _block plane_ (FIXME) marca Lie-Nielsen de 125 dólares podía encabronar a Jim.

Durante mi hora para comer ese día, atravesé a hurtadillas el pasillo hasta el taller de carpintería de la revista y afilé la cuchilla de acero del cepillo nuevo en el banco de alguién más. Quería desesperadamente inmiscuirme dentro del taller de la revista y de ganarme un espacio para trabajar. Pero, para hacer eso, necesitaba dejar de tomar prestadas sus herramientas, porque les molestaba encabronadamente.

Me había tomado meses ahorrar el dinero para el cepillo. Mi salario de $23,000 dólares anuales, que era patético aún para los años del 1990. Después de impuestos y seguro médico, llevaba a casa alrededor de $300 dólares por semana. Mi esposa, Lucy, también tenía un empleo de poca paga como periodista. Juntos teníamos una niña de un año, una hipotéca y un sorprendente gasto de guardería. Hubieramos estado financieramente mejor si me hubiese quedado en casa.

Pero el de _Popular Woodworking_ era un trabajo de ensueño. Yo era un entusiasta y poco notable carpintero casero con un par de títulos en periodismo y seis años de experiencia en periódicos y revistas. Acepté el trabajo en _Popular Woodworking_ porque estaba cansado de que me dispararan, gritaran y cuestionaran los de la policia estatal (todo era parte del trabajo cotidiano en los periódicos). Una revista de carpintería parecía un idilio ideal.

Cuando obtuve el trabajo en _Popular Woodworking_, mi papá me felicitó (a pesar de que secretamente quería que yo fuera abogado).

"Te encantará estar rodeado de carpinteros", dijo. "Esa gente es la sal de la tierra".

Acertó acerca de la parte salada.

Con el _block plane_ (FIXME) en mano, tenía ya suficientes herramientas como para poder hacer algo de trabajo serio de carpintería. Quería volver a hacer colas de milano (que aprendí a hacer tomando clases nocturnas en la Universidad de Kentucky). Pero no tenía un banco de carpintero en el taller de la revista. Así que, durante la comida solía meterme al taller para trabajar en el banco de alguien más y deshacerme de la evidencia para al finalizar la hora. Me toleraban pero no me alentaban. La descripción de mi trabajo era editar palabras, trabajar con diseñadores gráficos, agendar fotógrafos y cumplir con las fechas límite de la revista.

Para mi, ese trabajo de requería alrededor de 20 a 30 horas por semana. Y mi deseo por trabajar en el taller era tan intenso que rayaba en el dolor físico. Así, le pregunté a mi jefe, Steve, si podía tener un banco en el taller. Su respuesta: No hay bancos libres. ¿Podía yo construir un banco? Respuesta: "No es tu turno".

Como todas las revistas de carpintería, solíamos recibir cargamentos de herramientas gratis - mierda que ni siquiera habíamos solicitado. Los fabricantes de herramientas apostaban a que nos enamoráramos de sus chunches y escribiéramos alguna evaluación, generándoles publicidad casi-gratuita. Una caja aleatoria que recibimos contenía cuatro _brackets_ (FIXME) - No recuerdo si eran de plástico o de aluminio. Pero estaban diseñados para ser una manera rápida para hacer bancos para serruchar. Se cortaba pino en 2x4s a la media y se atornillaban a los _brackets_ (FIXME). _Voilà_ - se obtenían dos bancos para serruchar sin tener que cortar otra cosa que en 90° y sin tener que meterse con hacer ensambles.

Decidí que los bancos para serruchar podían ser la base de un banco de carpintero. Solamente tendría que hacer patas de 35 pulgadas de largo, en lugar de 24 pulgadas. Para la _benchtop_, me había hecho de una hermosa puerta Art Decó enchapada de nogal de nuestro almacén. La puerta había sido parte de un gran sistema de puertas plegables para la cafetería de nuestro edificio, del tiempo que fue planta de Coca-Cola.

Atornillé la puerta a los bancos para serruchar y pepené mi pequeña prensa Wilton para un extremo. Taladré algunos huecos para perros. Acabado.

Después de tan solo un día de trabajo, tenía un banco. Mientras nadie miraba, corrí a un lado la mesa del esmeril del taller y empujé mi banco a su sitio.

Y después de tan solo dos días de trabajo, tenía un banco al cual odiaba. Se corría, temblaba o deslizaba cada vez que yo serruchaba, cepillaba o usaba el formón en el. Cuando terminaba de cepillar una tabla, tenía que arrastrar 3 pies de regreso a la línea de inicio para cepillar otra tabla.

Raramente, nadie nos sacó ni a mi ni a mi banco al basurero. Jim fruncía el ceño, pero no decía nada. Había yo plantado mi bandera en el taller. Estaba dentro.

Y así, en la siguiente junta de planeación editorial para la revista, pregunté: ¿Puedo construir un banco?

Steve: "No es tu turno".

![](./ilustraciones/cap01-black-bar.png)

(FIXME - traducir pie de foto)

![](./ilustraciones/cap01-yellow-pine.png)

(FIXME - traducir pie de foto)
